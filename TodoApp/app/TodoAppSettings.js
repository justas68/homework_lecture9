import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from './actions';

class TodoAppSettings extends React.Component {
    constructor() {
        super();

        this.onChangeColor = this.onChangeColor.bind(this);
    }

    render() {
        if (this.props.loading) {
            return <div>loading...</div>;
        }

        return (
            <div className="app-color-picker">
                <div>
                    <span>App Color</span>
                </div>
                <input
                    type="color"
                    value={this.props.appSettings.appColor}
                    onChange={this.onChangeColor}
                />
            </div>
        );
    }

    onChangeColor(e) {
        this.props.changeColor(e.target.value)
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

const mapStateToProps = (state) => {
    return {appSettings: state.appSettings}
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoAppSettings)