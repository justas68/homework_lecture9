export default (state = {appColor:'#ff6666'}, action) => {
    switch (action.type) {
        case 'CHANGE_COLOR_SUCCESS':
            return {
                ...state,
                appColor: action.appColor
            };
        default:
            return state;
    }
};
