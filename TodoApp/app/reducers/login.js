export default (state = {}, action) => {
    switch (action.type) {
    case 'LOGIN_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'LOGIN_SUCCESS':
        return {
            ...state,
            loading: false,
            error: null
        };
    case 'LOGIN_ERROR':
        return {
            ...state,
            loading: false,
            error: action.error
        };
    default:
        return state;
    }
};