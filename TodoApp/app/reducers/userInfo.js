const defaultState = {
    loading: true
};

export default (state = defaultState, action) => {
    switch (action.type) {
    case 'GET_USER_INFO_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'GET_USER_INFO_SUCCESS':
        return {
            ...state,
            loading: false,
            userInfo: action.payload
        };
    case 'GET_USER_INFO_ERROR':
        return {
            ...state,
            loading: false,
            userInfo: null
        };
    default:
        return state;
    }
};