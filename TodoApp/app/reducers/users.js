const defaultState = {
    users: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'GET_USERS':
      return {
        ...state,
        users: action.users
      };
    case 'MAKE_USER_ADMIN':
      return{
      ...state,
      users: state.users.filter(user => user.name != action.userName).concat({name : action.userName, isAdmin : true}).sort((a, b) => a.name > b.name),
    }
    default:
        return state;
}
};