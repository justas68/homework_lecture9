import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import todoItems from './todoItems';
import appSettings from './appSettings';
import registration from './registration';
import login from './login';
import userInfo from './userInfo';
import logout from './logout';
import users from './users';

const rootReducer = combineReducers({todoItems, appSettings, registration, login, logout, users, userInfo, routing: routerReducer});

export default rootReducer;