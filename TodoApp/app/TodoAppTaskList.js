import React from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from './actions';

import TodoItem from './TodoItem';

class TodoAppTaskList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {newTitle: ''};

        this.onAddTask = this.onAddTask.bind(this);
        this.onUpdateTask = this.onUpdateTask.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onRemoveTask = this.onRemoveTask.bind(this);
    }

    componentDidMount() {
      this.props.getAppTodos();
    }

    render() {
        if (!this.props.userInfo) {
            return <div>Please log in to see your todos!</div>;
        }

        return (
            <div>
                <form onSubmit={this.onAddTask}>
                    <input type="text"
                        placeholder="What needs to be done?"
                        value={this.state.newTitle}
                        className="form-control"
                        onChange={this.onInputChange}
                    />
                    <input type="submit" hidden/>
                </form>
                <div className="list-group">
                    {this.renderItems()}
                </div>
            </div>
        );
    }

    renderItems() {
       return this.props.todoItems.map((todoItem, i) => {
            return (
                <TodoItem
                    key={todoItem.id}
                    index={todoItem.id}
                    completed={todoItem.completed}
                    title={todoItem.title}
                    onUpdateTask={this.onUpdateTask}
                    onRemoveTask={this.onRemoveTask}
                />
            );
        });
    }

    onInputChange(e) {
        this.setState({newTitle: e.target.value});
    }

    onAddTask (e) {
        e.preventDefault();
        this.props.addTodo(this.state.newTitle);
        this.setState({newTitle: ''});
    }

    onUpdateTask(item) {
        this.props.updateTodo(item);
    }

    onRemoveTask(taskId) {
       this.props.removeTodo(taskId);
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

const mapStateToProps = (state) => {
    return {
        todoItems: state.todoItems,
        userInfo: state.userInfo.userInfo
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoAppTaskList)