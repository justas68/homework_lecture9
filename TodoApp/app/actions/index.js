import storageService from '../services/storageService';
import {
    api
} from '../utils';
import {
    push
} from 'react-router-redux';

export const getAppTodos = () => (dispatch) => {
    api
        .get('/api/todos')
        .then(response => {
            dispatch({
                type: 'LOAD_TODOS',
                appTodos: response.data
            })
        });
};
export const getAllUsers = () => (dispatch) => {
    api
        .get('/api/admin')
        .then((response) => {
            dispatch({
                type: 'GET_USERS',
                users: response.data
            });
        });
};
export const makeUserAdmin = (name) => (dispatch) => {
    api
        .post('/api/admin/add?userName=' + name, null)
        .then((response) => {
            dispatch({
                type: 'MAKE_USER_ADMIN',
                userName: name,
            })
        });
};
export const addTodo = (title) => (dispatch) => {
    api
        .post('/api/todos', {
            title
        })
        .then((response) => {
            dispatch({
                type: 'ADD_TODO_SUCCESS',
                todo: response.data
            });
        });
};

export const removeTodo = id => (dispatch) => {
    api
        .delete(`/api/todos/${id}`)
        .then((response) => {
            if (!response.ok) return;
            dispatch({
                type: 'REMOVE_TODO_SUCCESS',
                id
            });
        });
};

export const updateTodo = todoItem => (dispatch) => {
    const {
        id,
        ...body
    } = todoItem;

    api
        .put(`/api/todos/${id}`, body)
        .then(() => {
            dispatch({
                type: 'UPDATE_TODO_SUCCESS',
                todoItem
            });
        });
}

export const changeColor = colorHexCode => (dispatch, getState) => {
    const currentSettings = getState().appSettings;

    storageService
        .updateAppSettings({ ...currentSettings,
            loading: false,
            appColor: colorHexCode
        })
        .then((response) => {
            dispatch({
                type: 'CHANGE_COLOR_SUCCESS',
                appColor: response.appColor
            });
        });
};

export const register = body => (dispatch) => {
    dispatch({
        type: 'REGISTER_REQUEST'
    });
    api
        .post('/api/account/register', body)
        .then((response) => {
            if (response.ok) {
                dispatch({
                    type: 'REGISTER_SUCCESS'
                });
                dispatch(push('/login'));
            } else {
                dispatch({
                    type: 'REGISTER_ERROR',
                    error: response.data.errorMessage || response.data
                });
            }
        });
};

export const loginAction = body => (dispatch) => {
    dispatch({
        type: 'LOGIN_REQUEST'
    });
    api
        .post('/api/account/login', body)
        .then((response) => {
            if (response.ok) {
                dispatch({
                    type: 'LOGIN_SUCCESS'
                });
                dispatch(push('/'));
                dispatch(getUserInfo);
            } else {
                dispatch({
                    type: 'LOGIN_ERROR',
                    error: response.data.errorMessage || JSON.stringify(response.data)
                });
            }
        });
};

export const getUserInfo = (dispatch) => {
    dispatch({
        type: 'GET_USER_INFO_REQUEST'
    });
    api
        .get('/api/account/me')
        .then((response) => {
            if (response.ok) {
                dispatch({
                    type: 'GET_USER_INFO_SUCCESS',
                    payload: response.data
                });
            } else {
                dispatch({
                    type: 'GET_USER_INFO_ERROR'
                });
            }
        });
};

export const logout = (dispatch) => {
    dispatch({
        type: 'LOGOUT_REQUEST'
    });
    api
        .post('/api/account/logout')
        .then((response) => {
            if (response.ok) {
                dispatch({
                    type: 'LOGOUT_SUCCESS'
                });
                dispatch(push('/login'));
                dispatch(getUserInfo);
            } else {
                dispatch({
                    type: 'LOGOUT_ERROR'
                });
            }
        });
};