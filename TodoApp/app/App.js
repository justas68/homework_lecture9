import { hot } from 'react-hot-loader';
import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';

import css from '../public/styles.css';
import TodoApp from './TodoApp';
import TodoAppTaskList from './TodoAppTaskList';
import TodoAppSettings from './TodoAppSettings';
import store, { history } from './store';
import Register from './components/Register';
import Login from './components/Login';
import Admin from './Users'

const App = () => (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={TodoApp}>
                <IndexRoute component={TodoAppTaskList} />
                <Route path="settings" component={TodoAppSettings} />
                <Route path="/register" component={Register} />
                <Route path="/login" component={Login} />
                <Route path="/admin" component={Admin} />
            </Route>
        </Router>
    </Provider>
);

export default hot(module)(App);