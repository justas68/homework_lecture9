import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { getUserInfo, logout } from './actions';


class TodoAppNavigation extends React.Component {

    constructor(props) {
        super(props);

        this.renderAuthorizedNavbar = this.renderAuthorizedNavbar.bind(this);
        this.renderUnauthorizedNavbar = this.renderUnauthorizedNavbar.bind(this);
        this.renderLoading = this.renderLoading.bind(this);
    }

     renderLoading()  {
        return (
            <div role="navigation" className="navbar navbar-default nav-center">
                <div className="navbar-header">
                    Loading...
                </div>
            </div>
        );
    }

 renderAuthorizedNavbar() {
        return (
            <div role="navigation" className="navbar navbar-default nav-center">
                <div className="navbar-header">
                    <Link to="/">Todos</Link>
                </div>
                <div className="navbar-header">
                    <Link to="/settings">Settings</Link>
                </div>
                <div className="navbar-header">
                    <a onClick={this.props.logout}>Log out</a>
                </div>
                <div className="navbar-header">
                    <Link to="/admin">Admin page</Link>
                </div>
            </div>
        );
    };

 renderUnauthorizedNavbar() {
        return (
            <div role="navigation" className="navbar navbar-default nav-center">
                <div className="navbar-header">
                    <Link to="/login">Log in</Link>
                </div>
                <div className="navbar-header">
                    <Link to="/register">Register</Link>
                </div>
            </div>
        );
    };




    componentDidMount() {
        this.props.getUserInfo();
    }


    render() {
        if (this.props.loading) {
            return this.renderLoading();
        }

        if (!this.props.userInfo) {
            return this.renderUnauthorizedNavbar();
        }

        return this.renderAuthorizedNavbar();
    }
}

const mapStateToProps = state => ({
    loading: state.userInfo.loading,
    userInfo: state.userInfo.userInfo
});

const mapDispatchToProps = dispatch => ({
    getUserInfo: () => dispatch(getUserInfo),
    logout: () => dispatch(logout)
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoAppNavigation);