import React from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from './actions';
import * as actions from './actions/index'
import PropTypes from 'prop-types';

export class Users extends React.Component {
constructor(props) {
    super(props);
    this.props.getAll();
}

  render() {
    return (
      <div>
        <ul>{this.props.users.map((user) =><li key = {user.name}>{user.name} <button disabled = {user.isAdmin} style={{marginLeft: "30%"} } onClick = {this.makeAdmin.bind(this)} name={user.name}>Make Admin</button></li>)}</ul>
      </div>
    )
};
  makeAdmin(e){
    event.preventDefault();
    this.props.makeUserAdmin(e.target.name);
  }
}

export default connect(
    (state) => ({
        users: state.users.users,
    }),
    (dispatch) => bindActionCreators({
        makeUserAdmin: actions.makeUserAdmin,
        getAll: actions.getAllUsers,
    }, dispatch)
)(Users);

Users.propTypes = {
    users: PropTypes.any,
    onUp: PropTypes.any
};