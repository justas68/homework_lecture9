﻿using System;
using System.Collections.Generic;
using System.Linq;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Enums;
using TodoApp.DataContracts.Requests;

namespace TodoApp.Data
{
  public class TodosRepository : ITodosRepository
  {
    private readonly AppDbContext _dbContext;
    public TodosRepository(AppDbContext appDbContext)
    {
      _dbContext = appDbContext;
    }

    public List<TodoItem> GetAll(GetAllTodosRequest request, int? userId = null)
    {
      var query = _dbContext.TodoItems.AsQueryable();

      if (userId.HasValue)
      {
        query = query.Where(x => x.UserId == userId);
      }

      if (!string.IsNullOrEmpty(request.Query))
      {
        query = query.Where(x => x.Title.ToLowerInvariant().Contains(request.Query.ToLowerInvariant()));
      }

      if (request.Completed.HasValue)
      {
        query = query.Where(x => x.Completed == request.Completed.Value);
      }

      if (request.SortDirection.HasValue)
      {
        query = request.SortDirection.Value == SortDirection.Asc
            ? query.OrderBy(x => x.Title)
            : query.OrderByDescending(x => x.Title);
      }

      return query.ToList();
    }

    public TodoItem GetById(int id)
    {
      return _dbContext.TodoItems.Single(x => x.Id == id);
    }

    public TodoItem Create(CreateTodoItemRequest request, int userId)
    {
      if (_dbContext.TodoItems.FirstOrDefault(x => x.Title.ToLowerInvariant() == request.Title.ToLowerInvariant()) != null)
      {
        throw new ArgumentException($@"Todo item with title ""{request.Title}"" already exists.");
      }

      var item = new TodoItem
      {
        Title = request.Title,
        UserId = userId,
        Completed = false
      };

      _dbContext.TodoItems.Add(item);

      _dbContext.SaveChanges();

      return item;
    }

    public bool Update(int id, UpdateTodoItemRequest request, int? userId = null)
    {
      var item = _dbContext.TodoItems.Single(x => x.Id == id);
      if (userId == null || item.UserId == userId)
      {
        item.Title = request.Title;
        item.Completed = request.Completed.GetValueOrDefault();

        _dbContext.SaveChanges();
        return true;
      }
      return false;
    }

    public bool Delete(int id, int? userId = null)
    {
      var item = _dbContext.TodoItems.Single(x => x.Id == id);
      if (userId == null || item.UserId == userId)
      {
        _dbContext.TodoItems.Remove(item);

        _dbContext.SaveChanges();

        return true;
      }
      return false;
    }
  }
}
