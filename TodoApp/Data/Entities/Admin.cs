using System.Collections.Generic;

namespace TodoApp.Data.Entities
{
    public class Admin
    {
        public string Name { get; set; }
        
        public bool IsAdmin {get; set; }
    }
}
