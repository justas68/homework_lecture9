﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;
using TodoApp.DataContracts.Responses;

namespace TodoApp.Controllers
{
  [Route("api/todos")]
  public class TodosController : Controller
  {
    private readonly ITodosRepository todosRepository;

    private readonly UserManager<User> _userManager;

    public TodosController(ITodosRepository todosRepository, UserManager<User> userManager)
    {
      this.todosRepository = todosRepository;

      _userManager = userManager;
    }
    [Authorize]
    [HttpGet]
    public async Task<IActionResult> GetAll([FromQuery] GetAllTodosRequest request)
    {
      var userIdClaim = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier);

      if (userIdClaim != null)
      {
        var userId = Int32.Parse(userIdClaim.Value);
        var roles = ((ClaimsIdentity)User.Identity).Claims
            .Where(c => c.Type == ClaimTypes.Role)
            .Select(c => c.Value);
        if (roles.Contains("Admin"))
        {
          return Ok(todosRepository.GetAll(request, null));
        }
        return Ok(todosRepository.GetAll(request, userId));
      }

      return NotFound(Enumerable.Empty<TodoItem>());
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
      try
      {
        return Ok(todosRepository.GetById(id));
      }
      catch (InvalidOperationException)
      {
        return NotFound();
      }
    }
    [Authorize]
    [HttpPost]
    public IActionResult CreateItem([FromBody]CreateTodoItemRequest request)
    {
      if (ModelState.IsValid)
      {
        try
        {
          var userIdClaim = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier);
          var userId = Int32.Parse(userIdClaim.Value);

          var item = todosRepository.Create(request, userId);

          return CreatedAtAction(nameof(GetById), item);

        }
        catch (ArgumentException e)
        {
          return StatusCode((int)HttpStatusCode.Conflict, new ErrorResponse { ErrorMessage = e.Message });
        }
      }
      else
      {
        return BadRequest(ModelState);
      }
    }
    [Authorize]
    [HttpPut("{id}")]
    public IActionResult UpdateItem(int id, [FromBody]UpdateTodoItemRequest request)
    {
      if (ModelState.IsValid)
      {
        try
        {
          var userIdClaim = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier);

          if (userIdClaim != null)
          {
            var userId = Int32.Parse(userIdClaim.Value);
            var roles = ((ClaimsIdentity)User.Identity).Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value);
            if (roles.Contains("Admin"))
            {
              todosRepository.Update(id, request, null);
            }
            else
            {
              if (!todosRepository.Update(id, request, userId))
              {
                return Unauthorized();
              }
            }
            return NoContent();
          }
          else
          {
            return NotFound();
          }
        }
        catch (InvalidOperationException)
        {
          return NotFound();
        }
      }
      else
      {
        return BadRequest(ModelState);
      }
    }
    [Authorize]
    [HttpDelete("{id}")]
    public IActionResult DeleteItem(int id)
    {
      if (ModelState.IsValid)
      {
        try
        {
          var userIdClaim = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier);

          if (userIdClaim != null)
          {
            var userId = Int32.Parse(userIdClaim.Value);
            var roles = ((ClaimsIdentity)User.Identity).Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value);
            if (roles.Contains("Admin"))
            {
              todosRepository.Delete(id, null);
            }
            else
            {
              if (!todosRepository.Delete(id, userId))
              {
                return Unauthorized();
              }
            }
            return NoContent();
          }
          else
          {
            return NotFound();
          }
        }
        catch (InvalidOperationException)
        {
          return NotFound();
        }
      }
      else
      {
        return BadRequest(ModelState);
      }
    }
  }
}
