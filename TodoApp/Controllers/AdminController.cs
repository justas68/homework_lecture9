﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;
using TodoApp.DataContracts.Responses;

namespace TodoApp.Controllers
{
    [Authorize]
    [Route("api/admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<User> _userManager;

        public AdminController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost("add")]
        public async Task<IActionResult> MakeAdmin([FromQuery] string userName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = await _userManager.FindByNameAsync(userName);
            await _userManager.AddToRoleAsync(user, "Admin");
            return Ok(user);
        }
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = _userManager.Users;
            var admins = new List<Admin>();
            foreach (var user in users)
            {
                var name = user.UserName;
                var roles = await _userManager.GetRolesAsync(user);
                admins.Add(new Admin
                {
                    Name = name,
                    IsAdmin = roles.Contains("Admin")
                });

            }
            return Ok(admins);
        }
    }
}
