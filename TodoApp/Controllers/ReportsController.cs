﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TodoApp.Data;
using TodoApp.Data.Entities;

namespace TodoApp.Controllers
{
    [Route("api/reports")]
    public class ReportsController : Controller
    {
        private readonly AppDbContext _dbContext;
        public ReportsController(AppDbContext appDbContext)
        {
            _dbContext = appDbContext;
        }
        
        [HttpGet("getTodos")]
        public List<TodoItem> GetTodos()
        {
            return _dbContext.TodoItems
                                .ToList();
        }
        
        
        [HttpGet("getUsersWithTasks")]
        public object GetUsersWithTasks()
        {
            var query = from user in _dbContext.Users
                        join todoItem in _dbContext.TodoItems
                            on user.Id equals todoItem.UserId
                        select new
                        {
                            UserId = user.Id,
                            Name = user.UserName,
                            TodoItemId = todoItem.Id,
                            TodoTitle = todoItem.Title,
                            IsComplete = todoItem.Completed
                        };

            var result = query.ToList();

            return result;
        }

        [HttpGet("getAllUsersWithTasks")]
        public object GetAllUsersWithTasks()
        {
            var query = from user in _dbContext.Users
                join todoItem in _dbContext.TodoItems
                    on user.Id equals todoItem.UserId
                    into MatchedTodoItems                                                               //
                from matchedTodoItem in MatchedTodoItems.DefaultIfEmpty()                               //
                select new
                {
                    UserId = user.Id,
                    Name = user.UserName,
                    TodoItemId = matchedTodoItem != null ? matchedTodoItem.Id.ToString() : "NULL",      //
                    TodoTitle = matchedTodoItem != null ? matchedTodoItem.Title : "NULL",               //
                    IsComplete = matchedTodoItem != null ? matchedTodoItem.Completed.ToString(): "NULL",//
                };

            var result = query.ToList();

            return result;
        }

        [HttpGet("getAllUsersWithTasksCount")]
        public object GetAllUsersWithTasksCount()
        {
           var query = from user in _dbContext.Users
            let cCount = user.TodoItems.Count()
            select new { Name = user.UserName, Count = cCount };

            var result = query.ToList();

            return result;
        }
    }
}
