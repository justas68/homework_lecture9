﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TodoApp.Migrations
{
    public partial class AddAdminRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[AspNetRoles] (ConcurrencyStamp, Name, NormalizedName)
                VALUES('B8E76E04-DCD3-42FE-A424-DFAE3579F955', 'Admin', 'ADMIN')
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [dbo].[AspNetRoles] WHERE [NormalizedName] = 'ADMIN'");
        }
    }
}
