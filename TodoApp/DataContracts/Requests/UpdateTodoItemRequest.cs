﻿using System.ComponentModel.DataAnnotations;

namespace TodoApp.DataContracts.Requests
{
    public class UpdateTodoItemRequest
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public bool? Completed { get; set; }
    }
}
