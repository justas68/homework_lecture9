﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApp.DataContracts.Responses
{
    public class UserInfoResponse
    {
        public string UserName { get; set; }

        public string Email { get; set; }
    }
}
