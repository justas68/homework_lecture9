﻿using System;
using System.Collections.Generic;
using TodoApp.Data.Entities;
using TodoApp.DataContracts.Requests;

namespace TodoApp.DataContracts
{
    public interface ITodosRepository
    {
        List<TodoItem> GetAll(GetAllTodosRequest request, int? userId = null);

        TodoItem GetById(int id);

        TodoItem Create(CreateTodoItemRequest request, int userId);

        bool Update(int id, UpdateTodoItemRequest request, int? userId);

        bool Delete(int id, int? userId);
    }
}